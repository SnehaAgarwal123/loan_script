/* eslint-disable no-console */
'use-strict';

/**
 * 
 * @param {String} message 
 * @param {Object | undefined } [object]
 */
exports.info = function (message, object) {
  if (object){
    console.log(`message:${message},object:${JSON.stringify(object)}`);
  } else {
    console.log(`message:${message}`);
  }
};

/**
 * @param {Error} errorObject
 * @param {String | undefined} [message]
 */
exports.error = function (errorObject, message) {
  if (message){
    console.error(`message:${message},errorMessage:${errorObject.message},errorStack:${errorObject.stack},error:${JSON.stringify(errorObject)}`);
  } else {
    console.error(`errorMessage:${errorObject.message},errorStack:${errorObject.stack},error:${JSON.stringify(errorObject)}`);
  }
};
