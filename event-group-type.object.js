const GroupedEventsTypes = {
    loan_initiated: 1,
    user_created: 1,
    new_application_initiated: 1,
    user_disqualified: 3,
    user_qualified: 2,
    loan_approved: 4,
    loan_rejected: 3,
    loan_esigned: 8,
    loan_closed: 6,
    loan_disbursed: 5,
    loan_cancelled: 7,
  };

  module.exports = GroupedEventsTypes;
  