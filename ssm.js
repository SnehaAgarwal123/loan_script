'use strict';

const SSM = require('aws-sdk/clients/ssm');

const Logger = require('./logger');

const parameterStore = new SSM({ region: 'ap-south-1' });

/**
 * @returns Promise<any>
 */
exports.getParam = async function(param, toDecrypt = true) {
  try {
    return (
      await parameterStore
        .getParameter({ Name: param, WithDecryption: toDecrypt })
        .promise()
    ).Parameter.Value;
  } catch (err) {
    Logger.error(`${err} Error while getting ${param} value from SSM.`);
    throw err;
  }
};
