'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Collections = {
    webhookevents: 'webhookevents',
};
const Schemas = {};
Schemas.webhookevents = new Schema(
  {
    userId: { type: Number, required: true, index: true },
    loanApplicationId: { type: String, sparse: true, index: true },
    entityType: { type: String },
    eventDescription: { type: String },
    eventType: { type: String },
    loggedAt: { type: String },
    journeyType: { type: String },
    source: { type: String },
  },
  { collection: Collections.webhookevents, timestamps: true },
);

module.exports = { Collections, Schemas };
