const csv = require('csv-parser')
const fs = require('fs')
const { connectToMongo } = require('./server');
const { Collections, Schemas } = require('./loan.schema');
const EventsTypes = require('./event-type.object');
const GroupedEventsTypes = require('./event-group-type.object');
const fetch = require('cross-fetch');
const logger = require('./logger');




(async function () {
    let results = [];
    let response = await fileReadAndUpload(results)
    // console.log('response', response)
    let docDb = await getDocDbConnection();
    await updateWebhookEvents(response, docDb);
    await updateLoanDetails(response, docDb);
})();

async function fileReadAndUpload(results) {
    const file = fs.createReadStream('C:\\Users\\TECHSPARE-14\\Downloads\\1641547997125.csv')
        .pipe(csv(
            {
                mapHeaders: ({ header }) => parseHeader(header),
            },
        ));

    return new Promise(function (resolve, reject) {
        file.on('data', (data) => results.push(data))
        file.on('end', () => {
            results = results.map((res) => {
                res.userId = Number(res.userId);
                res['webhookStatus'] = EventsTypes[res.eventType];
                res['status'] = GroupedEventsTypes[res.eventType];
                return res;
            });
            resolve(results);
        });


    });
}

async function getDocDbConnection() {
    let docDb = await connectToMongo('MONGO_APP_DB');
    return docDb;
}



async function updateWebhookEvents(jsonDump, docDb) {
    try {
        await docDb.connection.collection('webhookevents').insertMany(jsonDump)
            .then((res) => {
                console.log("insert success result ");
            })
            .catch(err => {
                console.log("bulk insert error ", err);
            });

    } catch (e) {
        console.log(e);
    }
}
async function groupBy(loanDetailsData) {
    let objectWithGroupByUser = {};
    for (let key in loanDetailsData) {
        let userId = loanDetailsData[key].userId;
        if (
            loanDetailsData[key].eventType === 'eligibility_calculated' ||
            loanDetailsData[key].eventType === 'web_link_generated' ||
            loanDetailsData[key].eventType === 'pan_verified'
        ) continue;
        let response = await makeFinboxCallsForEvents(loanDetailsData[key]);
        if (!objectWithGroupByUser[userId]) {
            response.status = compareStatus(response.webhookStatus);
            objectWithGroupByUser[userId] = response;
        } else {
            if (Date.parse(objectWithGroupByUser[userId].loggedAt) < Date.parse(response.loggedAt)) {
                objectWithGroupByUser[userId].status = response.status ? response.status : objectWithGroupByUser[userId].status;
                objectWithGroupByUser[userId].webhookStatus = response.webhookStatus ? response.webhookStatus : objectWithGroupByUser[userId].webhookStatus;
                objectWithGroupByUser[userId].status = compareStatus(objectWithGroupByUser[userId].webhookStatus);
                objectWithGroupByUser[userId].loggedAt = response.loggedAt;
            }
            if (response.userDetails) objectWithGroupByUser[userId].userDetails = response.userDetails;
            if (response.loanApplicationNum) objectWithGroupByUser[userId].loanApplicationNum = response.loanApplicationNum;
            if (response.appliedLoanAmount) objectWithGroupByUser[userId].appliedLoanAmount = response.appliedLoanAmount;
            if (response.loanDetailsCreatedAt) objectWithGroupByUser[userId].loanDetailsCreatedAt = response.loanDetailsCreatedAt;
            if (response.bankDetails) objectWithGroupByUser[userId].bankDetails = response.bankDetails;
            if (response.processingFee) objectWithGroupByUser[userId].processingFee = response.processingFee;
            if (response.disbursalAmount) objectWithGroupByUser[userId].disbursalAmount = response.disbursalAmount;
            if (response.gst) objectWithGroupByUser[userId].gst = response.gst;
            if (response.tenureMonths) objectWithGroupByUser[userId].tenureMonths = response.tenureMonths;
            if (response.annualInterest) objectWithGroupByUser[userId].annualInterest = response.annualInterest;
            if (response.lenderName) objectWithGroupByUser[userId].lenderName = response.lenderName;
        }
    };
    return objectWithGroupByUser;
}

function compareStatus(status) {
    if ((status >= 1 && status <= 7) || status == 56) {
        return 1;
    }
    if (status >= 9 && status <= 28) {
        return 2;
    }
    if (status >= 29 && status <= 41) {
        return 4;
    }
    if (status == 8 || status == 42) {
        return 3;
    }
    if (status == 44) {
        return 6;
    }
    if (status == 46) {
        return 7;
    }
    if (status == 45 || status == 47 || status == 48 || (status >= 50 && status <= 55)) {
        return 5;
    }
    if (status == 43 || status == 49) {
        return 8;
    }
}

async function makeFinboxCallsForEvents(body) {
    switch (body.eventType) {
        case 'loan_rejected':
            {
                const response = await callGetFinboxApi(
                    `/v1/loan/details`,
                    body.userId,
                    {
                        loanApplicationID: body.loanApplicationId,
                    },
                );
                let userDetails = {};
                userDetails.currentAddress = response.data.loanDetails.currentAddress;
                userDetails.loanFormData = response.data.loanDetails.loanFormData;
                userDetails.residenceType =
                    response.data.loanDetails.residenceType;
                userDetails.dob = response.data.loanDetails.dob;
                userDetails.email = response.data.loanDetails.email;
                userDetails.gender = response.data.loanDetails.gender;
                userDetails.name = response.data.loanDetails.name;
                userDetails.pan = response.data.loanDetails.pan;

                body.loanApplicationNum = response.data.loanApplicationNum;
                body.appliedLoanAmount = response.data.appliedLoanAmount;
                body.loanDetailsCreatedAt = response.data.createdAt;
                body.bankDetails = response.data.bankDetails;
                body.userDetails = userDetails;


                return body;
            }
        case 'loan_disbursed':
            {
                const loanD = callGetFinboxApi(`/v1/loan/details`, body.userId, {
                    loanApplicationID: body.loanApplicationId,
                });

                const loanO = callGetFinboxApi(`/v1/loan/offers`, body.userId, {
                    loanApplicationID: body.loanApplicationId,
                });

                const loanR = callGetFinboxApi(`/v1/loan/repayments`, body.userId, {
                    loanApplicationID: body.loanApplicationId,
                });

                const [loanDetails, loanOffers, loanRepayment] = await Promise.all([
                    loanD,
                    loanO,
                    loanR,
                ]);

                let userDetails = {};
                userDetails.currentAddress = loanDetails.data.loanDetails.currentAddress;
                userDetails.loanFormData = loanDetails.data.loanDetails.loanFormData;
                userDetails.residenceType = loanDetails.data.loanDetails.residenceType;
                userDetails.dob = loanDetails.data.loanDetails.dob;
                userDetails.email = loanDetails.data.loanDetails.email;
                userDetails.gender = loanDetails.data.loanDetails.gender;
                userDetails.name = loanDetails.data.loanDetails.name;
                userDetails.pan = loanDetails.data.loanDetails.pan;

                body.loanApplicationNum = loanDetails.data.loanApplicationNum;
                body.appliedLoanAmount = loanDetails.data.appliedLoanAmount;
                body.loanDetailsCreatedAt = loanDetails.data.createdAt;
                body.bankDetails = loanDetails.data.bankDetails;
                body.userDetails = userDetails;

                if (loanOffers && loanRepayment) {
                    body.processingFee = loanOffers.data[0].processingFee;
                    body.disbursalAmount = loanOffers.data[0].disbursalAmount;
                    body.gst = loanOffers.data[0].gst;
                    body.tenureMonths = loanOffers.data[0].tenureMonths;
                    body.annualInterest = loanOffers.data[0].annualInterest;
                    body.lenderName = loanRepayment.data.lenderName;
                }

                return body;
            }
        case 'loan_application_created':
            {
                const response = await callGetFinboxApi(
                    `/v1/user/profile`,
                    body.userId,
                );
                let userDetails = {};
                userDetails.pan = response.data.userProfile.pan;
                userDetails.name = response.data.userProfile.name;
                userDetails.gender = response.data.userProfile.gender;
                userDetails.email = response.data.userProfile.email;
                userDetails.dob = response.data.userProfile.dob;
                body.loanAppCreatedAt = body.loggedAt;
                body.userDetails = userDetails;

                return body;
            }

        case 'profile_updated':
            {
                const response = await callGetFinboxApi(
                    `/v1/user/profile`,
                    body.userId,
                );
                let userDetails = {};
                userDetails.pan = response.data.userProfile.pan;
                userDetails.name = response.data.userProfile.name;
                userDetails.gender = response.data.userProfile.gender;
                userDetails.email = response.data.userProfile.email;
                userDetails.dob = response.data.userProfile.dob;
                body.userDetails = userDetails;
                return body;
            }

        default: return body;
    }
}

async function callGetFinboxApi(url, id, params) {
    try {
        id = id.toString();

        if (url === '/v1/user/profile') {
            url = `https://lendingapis.finbox.in${url}?customerID=${id}`;
        } else {
            url = `https://lendingapis.finbox.in${url}?loanApplicationID=${params.loanApplicationID}`;
        }
        let res = await fetch(url, {
            headers: {
                'x-api-key': '9a8c329944d94048b20d97082e936123',
                'Content-Type': 'application/json',
            },
        })
        res = await res.json();

        return res;
    } catch (error) {
        console.log(error)
    }
}

async function updateLoanDetails(loanDetailsData, docDb) {
    const responseAfterGroup = await groupBy(loanDetailsData);
    for (let key in responseAfterGroup) {
        delete responseAfterGroup[key].userId;
        delete responseAfterGroup[key].entityType;
        delete responseAfterGroup[key].eventDescription;
        delete responseAfterGroup[key].eventType;
        delete responseAfterGroup[key].loggedAt;
        delete responseAfterGroup[key].journeyType;
        delete responseAfterGroup[key].source;
        delete responseAfterGroup[key]._id;
        if (responseAfterGroup[key].loanApplicationId === '') delete responseAfterGroup[key].loanApplicationId;
    }
    // console.log(results);
    const loan_model = await docDb.connection.model(
        Collections.loandetails,
        Schemas.loandetails,
    );
    const finalResponse = [];
    const objArray = [];
    Object.keys(responseAfterGroup).forEach(key => objArray.push({
        userId: key,
        update: responseAfterGroup[key]
    }));
    for (let i = 0; i < objArray.length; i++){
        logger.info(`current index  before updating the details in the db is: ${i}`);
        if (isNaN(objArray[i].userId)) continue;
        finalResponse.push(await loan_model.findOneAndUpdate({ userId: (Number(objArray[i].userId)) },
            objArray.update,
            { new: true },
        ));
    }
    console.log('Success: finalResponse', finalResponse)
}

function parseHeader(header, value) {
    switch (header) {
        case 'customer_id': header = 'userId';
            value = Number(value);
            break;
        case 'entity_type': header = 'entityType';
            break;
        case 'event_description': header = 'eventDescription';
            break;
        case 'journeytype': header = 'journeyType';
            break;
        case 'logged_at': header = 'loggedAt';
            break;
        case 'event_type': header = 'eventType';
            break;
        case 'loan_application_id': header = 'loanApplicationId';
    }
    return header;
}

