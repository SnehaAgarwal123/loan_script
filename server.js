'use strict';
const mongoose = require('mongoose');
const SSMUtil = require('./ssm');
const logger = require('./logger');

const cachedMongoDbInstances = {};
process.env.STAGE = process.env.STAGE || 'production';
async function connectToMongo(ssm_key) {
   try{
    const DB = {};
    let cachedMongoDb = cachedMongoDbInstances[process.env.STAGE];
    // eslint-disable-next-line no-constant-condition
    if (false && cachedMongoDb && cachedMongoDb.readyState === 1) {
      logger.info('=> using cached mongodb instance.');
      DB.connection = cachedMongoDbInstances[process.env.STAGE];
    } else {
      logger.info('=> connecting to mongodb.');
      const { host, port, username, password, db } =
        (await getMongoConnDetails(ssm_key)) || {};
      const MONGODB_URI = `mongodb://${host}:${port}/${db}?readPreference=primary&retryWrites=true&w=majority&ssl=false`;
      const options = {
        auth: {
          user: username,
          password: password,
        },
        poolSize: 2, // mongodb default is 5
        useNewUrlParser: true,
        useUnifiedTopology: true, // comment this if DB connection lost issue occurs
        useFindAndModify: false,
        useCreateIndex: true,
        // keepAlive: true,
        // bufferCommands: false, // Disable mongoose buffering
        // bufferMaxEntries: 0, // and MongoDB driver buffering
      };
      DB.connection = await mongoose.createConnection(MONGODB_URI, options);
      
      // set cached DB instance according to STAGE
      cachedMongoDbInstances[process.env.STAGE] = DB.connection;
    }
    return DB;
   }catch(e){
       console.log('error in connection', e)
  }
}
async function getMongoConnDetails(ssm_key) {
//   if (process.env.STAGE === 'production') {
//     return JSON.parse(await SSMUtil.getParam(ssm_key));
//   } else {
    return {
      host: 'localhost',
      port: '8900',
      username: 'snehaA',
      password: '$a$SnAg#Td#2021',
      db: 'vyapar_documentdb_staging',
    };
//   }
}

module.exports = { connectToMongo };
