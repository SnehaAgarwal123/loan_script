'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Collections = {
  loandetails: 'loandetails',
};
const Schemas = {};
Schemas.loandetails = new Schema(
  {
    loanApplicationId: String,
    userId: Number,
    companyUniqueId: String,
    loanApplicationNum: String,
    mobile: String,
    appliedLoanAmount: Number,
    status: Number,
    webhookStatus: Number,
    lenderName: String,
    loanAppCreatedAt: String,
    loanDetailsCreatedAt: String,
    disbursalAmount: Number,
    processingFee: Number,
    gst: Number,
    tenureMonths: Number,
    annualInterest: Number,
    userDetails: {
      name: String,
      email: String,
      gender: String,
      dob: String,
      pan: String,
      currentAddress: {
        line1: String,
        line2: String,
        city: String,
        pincode: String,
        state: String,
      },
        loanFormData: {
        dependents: String,
        educationLevel:String,
        expenses: String,
        fathersName: String,
        income: String,
        loanPurpose: String,
        maritalStatus: String,
        reference1Contact:String,
        reference1ContactName: String,
        reference1Name: String,
        reference1Relationship: String,
      },
      residenceType: String,
    },
    bankDetails: {
      accountNumber: String,
      ifscCode: String,
      bankName: String,
      accountHolderName: String,
    }
  },
  { collection: Collections.loandetails, timestamps: true },
);

module.exports = { Collections, Schemas };
